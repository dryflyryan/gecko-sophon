import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';

import './index.css';
import configureStore from 'data/Root/configureStore';
import initialState from 'stateData/initialState';

import App from './app/App';
import * as serviceWorker from './serviceWorker';

const { store } = configureStore(initialState);

const render = (Root) => {
  const root = document.getElementById('root');

  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <Root />
      </Provider>
    </AppContainer>,
    root,
  );
};

render(App);

if (module.hot && process.env.NODE_ENV === 'development') {
  module.hot.accept('./app/App', () => {
    // eslint-disable-next-line global-require
    const Root = require('./app/App');

    render(Root);
  });
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
