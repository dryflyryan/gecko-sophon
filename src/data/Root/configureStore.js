
import {
  applyMiddleware,
  combineReducers,
  compose,
  createStore,
} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import * as reducers from './rootReducer';

const composeEnhancers = (...args) => {
  return typeof window !== 'undefined'
    ? composeWithDevTools(...args)
    : compose(...args);
};

function configureStoreProd(initialState) {
  const rootReducer = combineReducers({ ...reducers });

  const middlewares = applyMiddleware();
  const enhancers = composeEnhancers(middlewares);

  const store = createStore(rootReducer, initialState, enhancers, middlewares);

  return { store };
}

function configureStoreDev(initialState) {
  const rootReducer = combineReducers({ ...reducers });

  const middlewares = applyMiddleware();
  const enhancers = composeEnhancers(middlewares);

  const store = createStore(rootReducer, initialState, enhancers);

  if (module.hot && process.env.NODE_ENV === 'development') {
    module.hot.accept('./rootReducer', () => {
      store.replaceReducer({ rootReducer });
    });
  }
  return { store };
}

const configureStore = process.env.NODE_ENV === 'production' ? configureStoreProd : configureStoreDev;

export default configureStore;
