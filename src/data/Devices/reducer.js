import initialState from 'stateData/initialState';

export default (state = initialState.devices, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
