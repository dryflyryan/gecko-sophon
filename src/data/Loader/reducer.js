import initialState from 'stateData/initialState';

export default (state = initialState.loader, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
