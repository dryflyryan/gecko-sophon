import styled from 'styled-components';

export const DashboardWrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: 100vh;
  max-height: 100vh;
`;

export const DashboardWindow = styled.div`
  flex: 1 1 100%;
`;
