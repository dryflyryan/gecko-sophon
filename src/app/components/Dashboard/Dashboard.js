import React from 'react';
import PropTypes from 'prop-types';

import {
  MenuBar,
} from 'common';

import {
  DashboardWindow,
  DashboardWrapper,
} from './Dashboard.styles';

const Dashboard = () => {
  return (
    <DashboardWrapper>
      <MenuBar />
      <DashboardWindow>

      </DashboardWindow>
    </DashboardWrapper>
  );
};

export default Dashboard;
