import React from 'react';
import PropTypes from 'prop-types';

import {
  LogoPanel,
} from './SubComponents';

import {
  MenuBarLarge,
  NavColumn,
  NavButton,
} from './MenuBar.styles';

const MenuBar = () => {
  return (
    <MenuBarLarge>
      <LogoPanel />
      <NavColumn>
        <NavButton>
          I am content
        </NavButton>
        <NavButton>
          I, too, am content
        </NavButton>
      </NavColumn>
    </MenuBarLarge>
  );
};

export default MenuBar;
