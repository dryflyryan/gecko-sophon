import styled from 'styled-components';
import {
  LogoPanel,
} from './SubComponents';

export const MenuBarLarge = styled.div`
  flex: 1 1 auto;
  background-color: rgb(229, 227, 239);
  width: 27.5%;
  max-width: 27.5%;
  max-height: 100%;
  display: grid;
  grid-template-columns: 20% auto 20%;
  grid-auto-rows: min-content;
`;

export const StyledLogoPanel = styled(LogoPanel)`
  display: grid;
  grid-column: 1 / span 3;
`;

export const NavColumn = styled.div`
  grid-column: 2;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  align-content: flex-start;
`;

export const NavButton = styled.button`
  box-sizing: border-box;
  margin: 2.5px 0;
  border: none;
  background-color: transparent;
  border-bottom: 1px solid transparent;
  cursor: pointer;
  :hover {
    border-bottom: 1px solid rgb(100, 100, 100);
  }
`;
