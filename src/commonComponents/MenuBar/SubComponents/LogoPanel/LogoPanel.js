import React from 'react';

import {
  LogoPanelContainer,
} from './LogoPanel.styles';

const LogoPanel = () => (
  <LogoPanelContainer>
    logo panel
  </LogoPanelContainer>
);

export default LogoPanel;
