import styled from 'styled-components';

export const LogoPanelContainer = styled.div`
  display: grid;
  grid-column: 1 / span 3;
  min-height: 300px;
`;
