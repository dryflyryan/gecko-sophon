const path = require('path');

module.exports = {
  app: path.resolve(__dirname, '../src/app'),
  common: path.resolve(__dirname, '../src/commonComponents'),
  components: path.resolve(__dirname, '../src/app/components'),
  constants: path.resolve(__dirname, '../src/constants'),
  data: path.resolve(__dirname, '../src/data'),
  src: path.resolve(__dirname, '../src'),
  stateData: path.resolve(__dirname, '../src/state'),
};
